var Pelicula = Backbone.Model.extend();
// Crear otra case que extienda de Pelicula
var PackPelicula = Pelicula.extend();
// Instanciacion
var pelicula1 = new Pelicula();
// peliculas1.titulo = titulo        pero para backbone automatice el desarrollo es necesario hacerlo con set:
pelicula1.set("titulo", "Armagedón");
// y para obtener usar la clave sin valor
pelicula1.get('titulo');
// Serialización en JSON
pelicula1.toJSON(); // {titulo: "Armagedón"}
pelicula1; // child {cid: "c1", attributes: {…}, _changing: false, _previousAttributes: {…}, changed: {…}, …}
