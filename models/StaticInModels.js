var ClaseModelo = Backbone.Model.extend(
    {
    propiedadInstancia: "Propiedad dentro de la instancia." 
    },
    {
        propDeClase: "Prop estatica",
        metodoDeClase: function () {
            return "Este es un método estático o método de clase.";
        }
    }
);

document.write('<strong>Sin instanciar la clase para los métodos y propiedades estáticos: </strong>' + ClaseModelo.metodoDeClase());