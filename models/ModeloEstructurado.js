// Estas propiedades o métodos dentro de la clase no están dentro de la estructura
// de backbone, sino javascript nativo
var Articulo = Backbone.Model.extend({
    id: 0,
    nombre: "",
    descripcion: "Sin descripción",
    date: "",
    precio: 0
});
var date = new Date();

var newArticulo = new Articulo();
newArticulo.set('id', 54483);
newArticulo.set("nombre", "Toalla de Baño");
newArticulo.set("descripcion", "Toalla para baños de hogares residenciales.");
newArticulo.set("date", date.getDay() + "/" + date.getMonth() + "/" + date.getFullYear());
newArticulo.set('precio', parseFloat(255.52));

document.write(
    '<strong>Id:</strong> ' + newArticulo.get('id') + '<br>' +
    '<strong>Nombre:</strong> ' + newArticulo.get('nombre') + '<br>' +
    '<strong>Descripción:</strong> ' + newArticulo.get('descripcion') + '<br>' +
    '<strong>Fecha de Entrada:</strong> ' + newArticulo.get('date') + '<br>' +
    '<strong>Precio:</strong> ' + newArticulo.get('precio') + '<br>'
);