//creo un objeto cualquiera con Javascript, usando notación JSON
var obj = {};
//extiendo ese objeto con la clase Backbone.Events
_.extend(obj, Backbone.Events);
// Sin espacio de nombre
obj.on('mi_primer_evento', function (msg) {
    console.log('Tienes un nuevo mensaje sin espacio de nombre: ' + msg);
});
// Con espacio de nombre
obj.on('evento:uno', function (msg) {
    console.log('Tienes un nuevo mensaje con espacio de nombre: ' + msg);
});
// Llamamos al evento con trigger y podemos pasarle parametros
obj.trigger('evento:uno', 'El mundo es cruel, por eso hay que servir de ejemplo al mismo.');
// Eliminar eventos
obj.off('mi_primer_evento');
// Ejecutar evento una sola vez
obj.once('nombre_nuevo_evento', function () {
//    code
});

// ----------------------------------------------------------
// Cambiar el ámbito de ejecución de un evento en Backbone.js
//creo un primer objeto
var fiatUno = {
marca: "Fiat"
}
//extiendo el objeto para que soporte eventos de Backbonejs
_.extend(fiatUno, Backbone.Events);
//creo un segundo objeto
var seatToledo = {
marca: "Seat"
}
//creo una función
function mostrarMarca(){
alert("La marca del objeto: " + this.marca);
}
//defino un evento sobre el primer objeto, cambiando el contexto de this
fiatUno.on("mievento", mostraMarca, seatToledo);
//disparo el evento sobre el primer objeto
fiatUno.trigger("mievento");
// ----------------------------------------------------------