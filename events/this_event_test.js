var persona = {
    nombre: 'Miguel',
    edad: 29,
    nacionalidad: 'Dominicana',
    estado: 'Activo'
};
//extiendo el objeto para que soporte eventos de backbone.js
_.extend(persona, Backbone.Events);
//asigno una función manejadora a un nuevo evento
persona.on('persona:first', function () {
    // console.log('this', this);
    document.write('<strong>Nombre:</strong> ' + this.nombre + '<br>');
    document.write('<strong>Edad:</strong> ' + this.edad + '<br>');
    document.write('<strong>Nacionalidad:</strong> ' + this.nacionalidad + '<br>');
    document.write('<strong>Estado:</strong> ' + this.estado + '<br>');
});
// trigger para ejecutar el evento
persona.trigger('persona:first');

